## [6.5.0] - 2019-05-26
###Added
- New ad events ad manifest, break start, break stop and ad quartile
- New parameters on the ads (givenAds, expectedAds, givenBreaks, expectedBreaks, breakNumber)

## [6.4.10] - 2019-05-26
###Added
- All extraparams are sent on session start request as well
###Fixed
- Different threads were accessing the same array to make RW operations and in some cases the index may not exist anymore, now this is prevented locking the array between operations

## [6.4.9] - 2019-05-29
###Added
- Mac OS support

## [6.4.8] - 2019-05-22
###Added
- Video events

## [6.4.7] - 2019-05-08
###Changed
- Fastdata url now points to a-fds.youborafds01.com

## [6.4.6] - 2019-04-24
###Added
- New content options have been added

## [6.4.5] - 2019-04-16
###Improved
- Now any Infinity request won't have code, sessionId will be used instead

## [6.4.4] - 2019-04-16
###Fixed
- Macro for SWIFT_VERSION now is set to 5.0

## [6.4.3] - 2019-04-15
###Added
- Now the parsing of Location header is supported
###Updated
- Update to Swift 5

## [6.4.2] - 2019-04-09
###Fix
- Optional Swift vars and params, set as nullable in Objective-C.

## [6.4.1] - 2019-04-02
###Add
- Renaming for customDimensions

## [6.4.0] - 2019-04-02
###Added
- Now is possible to delay the start event (and therefore have all the metadata ready) and have correcto joinTime

## [6.3.10] - 2019-03-13
###Ported
- YBChrono has been ported to Swift

## [6.3.9] - 2019-03-07
###Fixed
- Plugin now stops in case of error between /init and /start

## [6.3.8] - 2019-03-06
###Fixed
- Plus sign '+' is now sent correctly on any given parameter of the url

## [6.3.7] - 2019-02-20
###Added
- sessionRoot parameter is back for ALL requests

## [6.3.6] - 2019-02-18
###Fixed
- If using extraparamN they were not sent, now they are

## [6.3.5] - 2019-02-13
###Added
- Fingerprint parameter
- Account code on all requests
- AdError is sent if happens before init or start
###Fixed
- AKAMAI cdn parse should work as expected right now
###Improved
- Infinity url parameters are not send anymore if false
###Removed
- NQS6 transform

## [6.3.4] - 2019-02-06
###Added
- Auto ad Init
- Telefonica CDN
- New getProgram method
- New customDimensions method for content and ads
- App name and App release version options
###Fixed
- Correct time between retries
###Deprecated
- getTitle2
- Extra params

## [6.3.3] - 2019-01-24
###Fixed
- Improved support for carthage as a build and dependency system

## [6.3.2] - 2019-01-24
###Fixed
- In certain cases comm was reinstantiated and no event where send afterwards

## [6.3.1] - 2019-01-23
###Added
- Deprecate all ads completed method
###Fixed
- Playhead monitor with different playrate than 0 or 1

## [6.3.0] - 2019-01-21
###Added
- Automatic init

## [6.2.13] - 2018-01-16
###Fixed
- Apple TV now is named AppleTV 4G

## [6.2.12] - 2018-01-14
###Added
- Added pluginInfo paramter on session start too
- New iPhone models for device info
- Added AppleTV models

## [6.2.11] - 2018 -12-20
###Fixed
- Viewcode timestamp doesn't include "extra" zeros anymore
- Better auto background management

## [6.2.10] - 2018-12-03
###Changed
- Now autoDetectBackground option is set to true by default

## [6.2.9] - 2018-11-25
###Fixed
- An specific event flow was not adding all transforms when sending collected offline events

## [6.2.8] - 2018-11-21
###Fixed
- Since migration to SQLite v2 to support multithreading the local db was not being created

## [6.2.7] - 2018-11-20
###Fixed
- Several retain cycles Infinity related and memory leaks have been fixed

## [6.2.6] - 2018-11-07
###Fix
- Crash on iOS 12 due to thread management on sqlite database access

## [6.2.5] - 2018-10-30
###Improved
- Posible null pointer exceptions when passin nil to some Infinity public methods

## [6.2.4] - 2018-10-29
###Fixed
- Posible crash when getting nil sessionId

## [6.2.3] - 2018-10-09
###Fixed
- Crash when passing nil screenName

## [6.2.2] - 2018-10-03
###Updated
 - Swift version to support 4.2
###Added
- Add response code log

## [6.2.1] - 2018-08-27
###Fixed
- Correct view expiration behaviour

## [6.2.0] - 2018-08-20
###Added
- Support for Infinity

## [6.1.8] - 2018-07-30
###Improved
- Ping log now shows params
###Fix
- Household id getter now is set as nullable

##  [6.1.7] - 2018-05-04
###Added
- HouseholdId parameter

##  [6.1.6] - 2018-04-27
###Added
- Experiments Ids on options
- New parameters: latency, packetLoss, packetSend
- Option to obfuscateIp
- Option to disable seeks on live content
###Fixed
- Log typo

##  [6.1.5] - 2018-03-08
###Added
- Now there are ten extraparams (total twenty)

##  [6.1.4] - 2018-02-20
###Added
- Now is possible to fireError without init or start

##  [6.1.3] - 2018-02-20
###Fixed
- Wrong version number

##  [6.1.2] - 2018-02-20
###Fixed
- Wrong import name

##  [6.1.1] - 2018-02-20
###Improved
- Possible to send post request easly now too

##  [6.1.0] - 2018-01-30
###Added
- Device info to be more on device info displayed in youbora
- Offline mode
- New options added, User Type, Streaming protocol, Ad Extra params

##  [6.0.7] - 2017-12-22
###Added
- New AdError event

##  [6.0.6] - 2017-12-22
###Fixed
- FireFatalError checks

##  [6.0.5] - 2017-12-20
###Added
- AllAdsCompleted callback
- FireFatalError with exception
- Timemark on every request (just for debugging porpuses)
- FireStop on plugin
###Fixed
- AdInit count to ad total duration
- Pings stop when removing adapter
- FireFatalError at plugin level
- Stop not send if there's no init at least
- Playhead was not being send during ads
###Removed
- End and Stop flags

##  [6.0.5-beta] - 2017-11-24
###Added
- Stop after postrolls

##  [6.0.4] - 2017-11-15
###Fixed
- Effective playtime

##  [6.0.3] - 2017-11-14
###Fixed
- Ad count

##  [6.0.2] - 2017-11-07
###Added
- AdInit method
###Fixed
- adStart log string

##  [6.0.1] - 2017-11-03
###Added
- Support for tvos

##  [6.0.0] - 2017-10-04
###Added
- Autobackground detection added
###Fixed
- pauseDuration on /stop and /ping
